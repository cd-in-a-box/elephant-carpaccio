# Elephant Carpaccio

## Guide

- [Handout](https://docs.google.com/document/u/3/d/1TCuuu-8Mm14oxsOnlk8DqfZAA1cvtYu9WGv67Yj_sSk/pub)

## Commands

Setup
```
npm install
```

Tests:
```
test/run-e2e
npx jest
```

## User Stories

1
```
AS Seller
I WANT to get the product total
IN ORDER to apply taxes for one country
```
2
```
AS Seller
I WANT to get the taxable amount for a product
IN ORDER to provide to calculate final price
```
...
