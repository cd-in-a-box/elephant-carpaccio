const product = require('../src/domain/product')

test('it can apply tax rate for UT', () => {
    const productFixture = require('../test/fixtures/product.json')
    expect(product.applyTaxes(productFixture)).toBe(106.88)
});
