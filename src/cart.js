//
let cart = require('../test/fixtures/cart.json')

let cartTotal = 0
for (const i in cart) {
    cartTotal += (cart[i].quantity * cart[i].price)
}

console.info(`cartTotal: ${cartTotal}`)