//
function applyTaxes(product, taxPercentage = 6.88) {
    let lineTotal = (product.price * product.quantity)
    return (lineTotal) + (lineTotal / 100 * taxPercentage)
}

module.exports.applyTaxes = applyTaxes;
