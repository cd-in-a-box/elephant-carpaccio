//
const product = require('../test/fixtures/product.json')
const taxPercentage = 6.88

let lineTotal = (product.price * product.quantity)
let productTotal = (lineTotal) + (lineTotal / 100 * taxPercentage)

console.info(`lineTotal: ${lineTotal}, productTotal: ${productTotal}`)